#include "ResourceManager.h"
#include "../Renderer/ShaderProgram.h"

#include <sstream>
#include <fstream>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
//#include "../../../res/textures/stb_image.h"


ResourceManager::ResourceManager(const std::string& executablePath)
{
    size_t found = executablePath.find_last_of("/\\");
    m_path = executablePath.substr(0, found);
}

std::string ResourceManager::getFileString(const std::string& relativeFilePath) const
{
    std::ifstream f;
    f.open(m_path + "/" + relativeFilePath.c_str(), std::ios::in | std::ios::binary);
    if (!f.is_open())
    {
        std::cerr << "Failed to open file: " << std::endl;
        return std::string{};
    }

    std::stringstream buffer;
    buffer << f.rdbuf();
    return buffer.str();
}   

std::shared_ptr<Renderer::ShaderProgram> ResourceManager::loadShaderProgram(const std::string& shaderName, const std::string& vertexPath, const std::string& fragmentPath)
{
    std::string vertexString = getFileString(vertexPath);
    //std::cout << vertexString << std::endl;
    if (vertexPath.empty())
    {
        std::cerr << "No vertex shader!" << std::endl;
        return nullptr;
    }

    std::string fragmentString = getFileString(fragmentPath);
    //std::cout << fragmentString << std::endl;
    if (fragmentPath.empty())
    {
        std::cerr << "No fragment shader!" << std::endl;
        return nullptr;
    }

    std::shared_ptr<Renderer::ShaderProgram>& newShader = m_shaderPrograms.emplace(shaderName, std::make_shared<Renderer::ShaderProgram>(vertexString, fragmentString)).first->second;
    if (newShader->isCompiled())
    {
        return newShader;
    }

    std::cerr << "Can't load shader program:\n"
                << "Vertex: " << vertexPath << "\n"
                << "Fragment: " << fragmentPath << std::endl;
            
    return nullptr;
}


std::shared_ptr<Renderer::ShaderProgram> ResourceManager::getShader(const std::string& shader_name)
{
    ShaderProgramsMap::const_iterator it = m_shaderPrograms.find(shader_name);
    if (it != m_shaderPrograms.end())
    {
        return it->second;
    }  
    std::cerr << "Can't find the shader: " << shader_name << std::endl;
    return nullptr;
}

void ResourceManager::loadTextures(const std::string& textureName, const std::string& texturePath)
{
    int channel = 0;
    int width = 0;
    int height = 0;
}   
