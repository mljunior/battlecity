#include "ShaderProgram.h"
#include <iostream>
#include <vector>
#include <map>
#include <tuple>
#include <external/glm/glm/glm.hpp>

namespace Renderer {
    ShaderProgram::ShaderProgram(const std::string& vertexShader, const std::string& fragmentShader)
    {
        GLuint vertexShaderID;
        if (!createShader(vertexShader, GL_VERTEX_SHADER, vertexShaderID))
        {
            std::cerr << "VERTEX SHADER compile-time error" << std::endl;
            return;
        }

        GLuint fragmentShaderID;
        if (!createShader(fragmentShader, GL_FRAGMENT_SHADER, fragmentShaderID))
        {
            std::cerr << "FRAGMENT SHADER compile-time error" << std::endl;
            glDeleteShader(vertexShaderID);
            return;
        }

        m_ID = glCreateProgram();
        glAttachShader(m_ID, vertexShaderID);
        glAttachShader(m_ID, fragmentShaderID);
        glLinkProgram(m_ID);

        GLint success;
        glGetProgramiv(m_ID, GL_LINK_STATUS, &success);
        if (!success)
        {
            GLchar infolog[1024];
            glGetProgramInfoLog(m_ID, 1024, nullptr, infolog);
            std::cerr << "ERROR::SHADER: Link-time error:\n" << infolog << std::endl;
        }
        else
        {
            m_isCompiled = true;
        }

    }

    bool ShaderProgram::createShader(const std::string& source, const GLuint shaderType, GLuint& shaderID)
    {
        shaderID = glCreateShader(shaderType);
        const char* code = source.c_str(); 
        glShaderSource(shaderID, 1, &code, nullptr);
        glCompileShader(shaderID);


        GLint success;
        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            GLchar infolog[1024];
            glGetProgramInfoLog(shaderID, 1024, nullptr, infolog);
            std::cerr << "ERROR::SHADER: compile-time error:\n" << infolog << std::endl;
            return false;
        }
        return true;
    }
    int ShaderProgram::getUniformLocation(const GLchar* name) const
    {
        return glGetUniformLocation(m_ID, name);
    }

    ShaderProgram::~ShaderProgram()
    {
        glDeleteShader(m_ID);
    }

    void ShaderProgram::use() const
    {
        glUseProgram(m_ID);
        
    }

    void ShaderProgram::setColors(std::vector<GLfloat> colors)
    {
        for (auto elem: colors)
        {
            verticesColors.push_back(elem);
        }
    }

    ShaderProgram& ShaderProgram::operator=(ShaderProgram&& ShaderProgram) noexcept
    {
        glDeleteProgram(m_ID);   
        m_ID = ShaderProgram.m_ID;
        m_isCompiled = ShaderProgram.m_isCompiled;

        ShaderProgram.m_ID = 0;
        ShaderProgram.m_isCompiled = false;
        return *this;
    }

    ShaderProgram::ShaderProgram(ShaderProgram&& ShaderProgram) noexcept
    {
        bool m_isCompiled = false;
        GLuint m_ID = 0;
    }

    // Полезные uniform-функции
    void ShaderProgram::setBool(const std::string &name, bool value) const
    {         
        glUniform1i(glGetUniformLocation(m_ID, name.c_str()), (int)value); 
    }
    void ShaderProgram::setInt(const std::string &name, int value) const
    { 
        glUniform1i(glGetUniformLocation(m_ID, name.c_str()), value); 
    }
    void ShaderProgram::setFloat(const std::string &name, float value) const
    { 
        glUniform1f(glGetUniformLocation(m_ID, name.c_str()), value); 
    }
    
    void ShaderProgram::setMat4(const std::string& name, const glm::mat4& mat) const
    {
        glUniformMatrix4fv(glGetUniformLocation(m_ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
    }
    void ShaderProgram::setVec3(const std::string& name, const glm::vec3& value) const
    {
        glUniform3fv(glGetUniformLocation(m_ID, name.c_str()), 1, &value[0]);
    }
    void ShaderProgram::setVec3(const std::string& name, float x, float y, float z) const
    {
        glUniform3f(glGetUniformLocation(m_ID, name.c_str()), x, y, z);
    }
    void ShaderProgram::setVec4(const std::string& name, const glm::vec4& value) const
    {
        glUniform4fv(glGetUniformLocation(m_ID, name.c_str()), 1, &value[0]);
    }
    void ShaderProgram::setVec4(const std::string& name, float x, float y, float z, float w) const
    {
        glUniform4f(glGetUniformLocation(m_ID, name.c_str()), x, y, z, w);
    }
}