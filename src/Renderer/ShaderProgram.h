#pragma once
#include <string>
#include <glad/glad.h>
#include <vector>
#include <map>
#include <string>
#include <GLFW/glfw3.h>
#include <external/glm/glm/glm.hpp>

namespace Renderer {
    class ShaderProgram {
        public:
            ShaderProgram(const std::string& vertexShader, const std::string& fragmentShader);
            ~ShaderProgram();
            bool isCompiled() const {return m_isCompiled; }
            void use() const;
            void setColors(std::vector<GLfloat> colors);

            ShaderProgram() = delete;
            ShaderProgram(ShaderProgram&) = delete;
            ShaderProgram& operator=(const ShaderProgram&) = delete;
            ShaderProgram& operator=(ShaderProgram&&) noexcept;
            ShaderProgram(ShaderProgram&& ShaderProgram) noexcept;
            
            // Полезные uniform-функции
            void setBool(const std::string &name, bool value) const;  
            void setInt(const std::string &name, int value) const;   
            void setFloat(const std::string &name, float value) const;
            int getUniformLocation(const GLchar* name) const;
            void setMat4(const std::string& name, const glm::mat4& mat) const;
            void setVec3(const std::string& name, const glm::vec3& value) const;
            void setVec3(const std::string& name, float x, float y, float z) const;
            void setVec4(const std::string& name, const glm::vec4& value) const;
            void setVec4(const std::string& name, float x, float y, float z, float w) const;

        private:
            bool createShader(const std::string& source, const GLuint shaderType, GLuint& shaderID);
            
            bool m_isCompiled;
            GLuint m_ID;
            std::vector <GLfloat> verticesColors = {
            1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f
        };
            std::vector<const GLchar*> colorVec = { "firstVertexColor", "secondVertexColor", "thirdVertexColor" };
    };
}